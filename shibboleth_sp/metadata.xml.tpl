<?xml version="1.0" encoding="utf-8" ?>
<md:EntityDescriptor xmlns:md="urn:oasis:names:tc:SAML:2.0:metadata">
	<md:SPSSODescriptor>
  	</md:SPSSODescriptor>
 
	<md:Organization>
		<md:OrganizationName xml:lang="en">${SHIB_ORG_NAME}</md:OrganizationName>
		<md:OrganizationDisplayName xml:lang="en">${SHIB_ORG_NAME}</md:OrganizationDisplayName>
	    <md:OrganizationURL xml:lang="en">${SHIB_ORG_URL}</md:OrganizationURL>
	</md:Organization>

	<md:ContactPerson contactType="technical">
    	<md:GivenName>Helpdesk</md:GivenName>
    	<md:SurName>Helpdesk</md:SurName>
    	<md:Company>${SHIB_ORG_ICT}</md:Company>
    	<md:EmailAddress>${SHIB_ORG_ICT_HELPDESK}</md:EmailAddress>
	</md:ContactPerson>
</md:EntityDescriptor>
