#!/bin/bash
# Starts a container for current user


#    --env "SHIB_SP_ATTRIBUTE_PREFIX=fit_" \


docker run -ti --rm \
    --env "SHIB_SP_HOSTNAME=test.www.fit.cvut.cz" \
    --env "SHIB_SP_PREFIX=app" \
    --env "SHIB_SP_SUPPORT=helpdesk@fit.cvut.cz" \
    --env "SHIB_IDP_ENTITY_ID=https://idp.fit.cvut.cz/idp/shibboleth" \
    --env "SHIB_IDP_METADATA_URL=https://idp.fit.cvut.cz/metadata/fit-metadata.xml" \
    --env "SHIB_IDP_ATTRIBUTE_MAP=http://idp.fit.cvut.cz/metadata/attribute-map.xml" \
    --publish="9001:9001" \
    --publish="9002:9002" \
    registry.gitlab.com/tomaskadlec/container-shibboleth:latest
