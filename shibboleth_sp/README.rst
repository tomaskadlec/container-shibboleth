shibboleth_sp image
===================

.. _`shibboleth`: shibboleth/README.rst
.. _`Debian testing`: https://wiki.debian.org/DebianTesting
.. _`Nginx`: https://nginx.com
.. _`PHP`: https://php.net
.. _`Shibboleth SP`: https://shibboleth.net/products/service-provider.html
.. _`nginx-http-shibboleth`: https://github.com/nginx-shib/nginx-http-shibboleth

Usage
-----

The image requires several variables to configure a container to specific needs.
If any of required variables is empty container fails to start. The same applies if
it is not possible to download attribute map

Upon successful start, the container exposes ``shibauthorizer`` on port tcp/9001
and ``shibresponder`` on tcp/9002 for use with `nginx-http-shibboleth`_.

Configuration
~~~~~~~~~~~~~

Shibboleth SP creates a metadata that must be registered within Shibboleth IdP
in order to authenticate users. Metadata template is present in the image. It
can be altered using following variables:

``SHIB_ORG_NAME``
    Name of organization, e.g. Faculty of Information Technology Czech Technical
    University

``SHIB_ORG_URL``
    Web address of the organization, e.g. http://www.fit.cvut.cz

``SHIB_ORG_ICT``
    Name of organizational unit responsible of handling ICT, e.g. Office of ICT
    Services Faculty of Information Technology Czech Technical University

``SHIB_ORG_ICT_HELPDESK``
    Support e-mail contact, e.g. helpdesk@fit.cvut.cz

If necessary it is possible to provide a custom template. Just mount it to
``/etc/shibboleth/metadata.xml``.

Following variables will be substituted in Shibboleth SP configuration file
(``/etc/shibboleth/shibboleth2.xml``).

``SHIB_HOSTNAME`` (required)
    Hostname of the service provider (SP) in question. It will be used in entity
    ID and in a ``RequestMap`` to identify which URLs are handled by the SP.

``SHIB_IDP_ENTITY_ID`` (required)
    IdP entity ID

``SHIB_IDP_METADATA_URL`` (required)
    An URL where metadata are located. Metadata are reloaded each 2 hours.

``SHIB_IDP_ATTRIBUTE_MAP``
    An URL where an attribute map is located. If no URL is given an attribute
    map must be mounted to ``/etc/shibboleth/attribute-map.xml`` (option
    ``--volume`` to ``docker run``).

``SHIB_ATTRIBUTE_PREFIX``
    A prefix used by Shibboleth for all attributes. Defaults to an empty string.

Starting a container
~~~~~~~~~~~~~~~~~~~~

To start a container a SSL/TLS key-certificate pair is necessary. If no such
pair was provided (i.e. using ``--volume`` option to ``docker run``) entrypoint
script will create one. Please note that such pair is suitable for testing
purposes only.

An example invocation is shown in the following excerpt ::

    docker run -ti --rm \
        --env="SHIB_HOSTNAME=obedar.fit.cvut.cz" \
        --env="SHIB_ATTRIBUTE_PREFIX=fit_" \
        --env="SHIB_ORG_ICT_HELPDESK=helpdesk@fit.cvut.cz" \
        --env="SHIB_IDP_ENTITY_ID=https://idp.fit.cvut.cz/idp/shibboleth" \
        --env="SHIB_IDP_METADATA_URL=https://idp.fit.cvut.cz/metadata/fit-metadata.xml" \
        --env="SHIB_IDP_ATTRIBUTE_MAP=https://idp.fit.cvut.cz/metadata/attribute-map.xml" \
        --publish="9001:9001" \
        --publish="9002:9002" \
        --volume=/obedar/ssl:/etc/ssl/app \
        registry.gitlab.com/tomaskadlec/container-shibboleth/shibboleth_sp:latest

Container architecture
----------------------

The image uses `supervisor`_ to run ``shibauthorizer`` and ``shibresponder`` as
FastCGI applications. ``shibd`` is executed on foreground as PID 1.

.. vim: spelllang=en spell textwidth=80 fo-=l: 
.. reformat paragraphs to 80 characters: select using v then hit gq
