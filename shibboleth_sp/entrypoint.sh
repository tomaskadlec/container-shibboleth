#!/bin/bash
# shibboleth container entrypoint

# check if required variables are set
VALIDATION_FAILED=0

if [ -z "$SHIB_HOSTNAME" ]; then
    echo "container_init: ENTRYPOINT: SHIB_HOSTNAME is required." 2>&1
    VALIDATION_FAILED=1
fi

if [ -z "$SHIB_ORG_ICT_HELPDESK" ]; then
    echo "container_init: ENTRYPOINT: SHIB_ORG_ICT_HELPDESK is required." 2>&1
    VALIDATION_FAILED=1
fi    

if [ -z "$SHIB_IDP_ENTITY_ID" ]; then
    echo "container_init: ENTRYPOINT: SHIB_IDP_ENTITY_ID is required." 2>&1
    VALIDATION_FAILED=1
fi    

if [ -z "$SHIB_IDP_METADATA_URL" ]; then
    log "container_init: ENTRYPOINT: SHIB_IDP_METADATA_URL is required." 2>&1
    VALIDATION_FAILED=1
fi    

if [ "$VALIDATION_FAILED" != "0" ]; then
    echo "container_init: ENTRYPOINT: Failed to start container." 2>&1
    exit 1
fi    

# replace variables
for FILE in /etc/shibboleth/*.tpl; do
    cat "$FILE" | envsubst > "${FILE/.tpl/}"
done    

# generate self-signed certificate if necessary
if [ ! -f /etc/ssl/app/key.pem ]; then
    mkdir -p /etc/ssl/app
    openssl req -x509 -newkey rsa:4096 -days 356 -nodes \
        -keyout /etc/ssl/app/key.pem \
        -out /etc/ssl/app/crt_ca.pem \
        -config /etc/shibboleth/openssl.cnf
    ln -s /etc/ssl/app/crt_ca.pem /etc/ssl/app/crt.pem
fi

# download attribute map
if [ -n "$SHIB_IDP_ATTRIBUTE_MAP" ]; then
    wget --no-check-certificate -O /etc/shibboleth/attribute-map.xml "$SHIB_IDP_ATTRIBUTE_MAP" || {
        echo "container_init: ENTRYPOINT: Failed to download attribute map" 2>&1;
        exit 2;
    }
fi

# remove empty attribute attributePrefix
sed -i 's/attributePrefix=""//' /etc/shibboleth/shibboleth2.xml

# run CMD
exec "$@"
