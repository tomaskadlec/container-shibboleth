container-shibboleth
====================

.. _`shibboleth`: shibboleth/README.rst
.. _`Debian Stretch`: https://wiki.debian.org/DebianStretch
.. _`Nginx`: https://nginx.com
.. _`PHP`: https://php.net
.. _`Shibboleth SP`: https://shibboleth.net/products/service-provider.html
.. _`nginx-http-shibboleth`: https://github.com/nginx-shib/nginx-http-shibboleth

This project builds two images that are necessary to support Shibboleth SSO.
Both images are build on `Debian Stretch`_ image.

The first one is `Shibboleth SP`_. Processes ``shibd``,``shibauthorizer`` and
``shibresponder`` are running in it and listening on TCP sockets for incoming
connections to handle authentication requests. Thus the image is ready for use
with with nginx_ and `nginx-http-shibboleth`_.

The second image is customized installation of nginx_ webserver with
`nginx-http-shibboleth`_ built in.

Please refer to each image README file for information how to use it:

* `shibboleth-sp <shibboleth_sp/README.rst>`_,
* `nginx <nginx/README.rst>`_.

License
-------

.. _`MIT License`:

This project is licensed under `MIT License`_

.. vim: spelllang=en spell textwidth=80 fo-=l: 
.. reformat paragraphs to 80 characters: select using v then hit gq
