nginx image
===========

.. _`shibboleth`: shibboleth/README.rst
.. _`Debian testing`: https://wiki.debian.org/DebianTesting
.. _`Nginx`: https://nginx.com
.. _`PHP`: https://php.net
.. _`Shibboleth SP`: https://shibboleth.net/products/service-provider.html
.. _`nginx-http-shibboleth`: https://github.com/nginx-shib/nginx-http-shibboleth

The image contains Nginx_ web server compiled with `headers-more-nginx-module`_
and `nginx-http-shibboleth`_ module. It is built to run `Symfony
<http://symfony.com>`_ applications. If you need to run anything else you are
required to provide alternate configuration for Nginx_ (e.g. using ``--volume``
option when starting a container). 

Usage
-----

Configuration variables
~~~~~~~~~~~~~~~~~~~~~~~

The image requires several environment variables to configure Nginx_ and
optionally to create a TLS key-pair. If any of required variables is empty
container fails to start. 

``NGX_HOSTNAME`` 
    Public DNS name of applicaton running in the container, e.g.
    obedar.fit.cvut.cz.

``NGX_APP``
    Hostname of a host where php-fpm is running and listening on tcp/9000, e.g.
    ``obedar-app``. If application runs as a container ``--link`` option to
    ``docker run`` with the same name must be provided. 

``NGX_SHIBBOLETH``
    Hostname of a host where shibboleth is running and listening on tcp/9001 and
    tcp/9002, e.g. ``obedar-shibboleth``. If application runs as a container
    ``--link`` option to ``docker run`` with the same name must be provided. 

Starting a container
~~~~~~~~~~~~~~~~~~~~

To start a container a SSL/TLS key-certificate pair is necessary. If no such
pair was provided (i.e. using ``--volume`` option to ``docker run``) entrypoint
script will create one. Please note that such pair is suitable for testing
purposes only.

Nginx_ must serve static content (e.g. css and js files, images) to clients.
However, application is available in application container only. Thus,
application must be defined as a volume of application container. Such volume
may be mounted by other containers and this container must mount it. 

Upon successful start, Nginx_ listens on tcp/443. Error log is forwarded to
``/dev/stderr`` and access log to ``/dev/stdout`` which are handled by Docker.
You can access logs using: ::

    docker logs CONTAINER_NAME

An example invocation is shown in the following excerpt ::

    docker run -ti --rm \
        -e NGX_HOSTNAME=obedar.fit.cvut.cz \
        -e NGX_APP=obedar-app \
        -e NGX_SHIBBOLETH=obedar-shibboleth -p 443:443 \
        --link "obedar-app" \
        --link "obedar-shibboleth" \
        --volume /obedar/ssl:/etc/ssl/app \
        --volumes-from obedar-app \
        registry.gitlab.com/tomaskadlec/container-shibboleth/nginx:latest

Container architecture
----------------------

``nginx`` master process is started on foreground with PID 1.

.. vim: spelllang=en spell textwidth=80 fo-=l: 
.. reformat paragraphs to 80 characters: select using v then hit gq
