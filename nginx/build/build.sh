#!/bin/bash -ex
# build nginx-custom package with http-shibboleth

SCRIPT=$(readlink -f "$0")
SCRIPT_DIR=$(dirname "$SCRIPT")

cat > /etc/apt/sources.list.d/src.list << -SRC-
deb-src http://deb.debian.org/debian stretch main
deb-src http://security.debian.org/debian-security stretch/updates main
-SRC-

NGINX_GPGKEY=573BFD6B3D8FBC641079A6ABABF5BD827BD9BF62;

apt-get update
apt-get install --no-install-recommends --no-install-suggests -y gnupg1 
apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-keys "$NGINX_GPGKEY"

cat >> /etc/apt/sources.list.d/src.list << -SRC-
deb http://nginx.org/packages/mainline/debian/ stretch nginx
deb-src http://nginx.org/packages/mainline/debian/ stretch nginx
-SRC-

apt update
apt install -y dpkg-dev wget

cd /tmp/nginx

rm -rf nginx* || true

apt source -y nginx
apt build-dep -y nginx

#cd $(ls -lad nginx* | grep '^d' | head -1 | sed 's/[[:space:]]\+/ /' | cut -d ' ' -f 11)
cd nginx-[1-9.]*

# Download latest version of nginx module
# TODO A better way would be to query API to get archive URL. The module does not 
#      have any releases so tags endpoint must be queried. The problem is that no
#      time related information is provided in the response. It's hard to identify
#      the last tag. Thus parsing releases webpage is much simpler even there is
#      no guarantee it won't change
function download_module {
    local MODULE_NAME="$1"
    local MODULE_URL="$2"
    local MODULE_TGZ=$(wget -O - "$MODULE_URL" | grep -o '"[^"]*.tar.gz"' | head -n1 | tr -d \")
    MODULE_TGZ="https://github.com${MODULE_TGZ}"
    wget -O "$MODULE_NAME.tgz" "$MODULE_TGZ"
    tar xzf "$MODULE_NAME.tgz"
    mv "${MODULE_NAME}-"* "$MODULE_NAME"
}
download_module nginx-http-shibboleth "https://github.com/nginx-shib/nginx-http-shibboleth/releases"
download_module headers-more-nginx-module "https://github.com/openresty/headers-more-nginx-module/releases"

# Update rules for building packages
sed -i 's|\(^[[:space:]]\+\(CFLAGS="" \)\?[.]/configure.*$\)|\1 --add-module=$(CURDIR)/headers-more-nginx-module --add-module=$(CURDIR)/nginx-http-shibboleth|' debian/rules

dpkg-buildpackage -b

