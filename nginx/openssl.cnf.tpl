default_bits            = 2048
distinguished_name      = req_distinguished_name
string_mask             = nombstr
prompt                  = no

[req_distinguished_name]
countryName             = CZ
commonName              = $NGX_HOSTNAME
