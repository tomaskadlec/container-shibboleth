#!/bin/bash
# nginx container entrypoint

# check if required variables are set
VALIDATION_FAILED=0

if [ -z "$NGX_HOSTNAME" ]; then
    echo "container_init: ENTRYPOINT: Required variable NGX_HOSTNAME is not set." 2>&1
    VALIDATION_FAILED=1
fi

if [ -z "$NGX_APP" ]; then
    echo "container_init: ENTRYPOINT: Required variable NGX_APP is not set." 2>&1
    VALIDATION_FAILED=1
fi

if [ -z "$NGX_SHIBBOLETH" ]; then
    echo "container_init: ENTRYPOINT: Required variable NGX_SHIBBOLETH is not set." 2>&1
    VALIDATION_FAILED=1
fi

if [ "$VALIDATION_FAILED" != "0" ]; then
    echo "container_init: ENTRYPOINT: Failed to start container." 2>&1
    exit 1
fi    

# replace variables
for FILE in /etc/nginx/*.tpl; do
    cat "$FILE" | envsubst '$NGX_HOSTNAME:$NGX_APP:$NGX_SHIBBOLETH' > "${FILE/.tpl/}"
done    

# generate self-signed certificate if necessary
if [ ! -f /etc/ssl/app/key.pem ]; then
    mkdir -p /etc/ssl/app
    openssl req -x509 -newkey rsa:4096 -days 356 -nodes \
        -keyout /etc/ssl/app/key.pem \
        -out /etc/ssl/app/crt_ca.pem \
        -config /etc/nginx/openssl.cnf
    openssl dhparam -dsaparam -out '/etc/ssl/app/dh.pem' 4096
fi

# run CMD
exec "$@"
