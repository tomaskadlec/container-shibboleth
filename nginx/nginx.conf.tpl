# /etc/nginx/nginx.conf
# nginx configuration for Symfony applications

user www-data www-data;
worker_processes auto;

error_log /var/log/nginx/error_log info;

events {
    worker_connections 1024;
    use epoll;
}

http {
    include /etc/nginx/mime.types;
    default_type application/octet-stream;

    log_format main
        '$remote_addr - $remote_user [$time_local] '
        '"$request" $status $bytes_sent '
        '"$http_referer" "$http_user_agent" '
        '"$gzip_ratio"';

    access_log /var/log/nginx/access_log main;

    client_header_timeout 10m;
    client_body_timeout 10m;
    send_timeout 10m;

    connection_pool_size 256;
    client_header_buffer_size 4k;
    large_client_header_buffers 4 4k;
    request_pool_size 4k;

    gzip on;
    gzip_min_length 1100;
    gzip_buffers 4 8k;
    gzip_types text/plain;

    output_buffers 1 32k;
    postpone_output 1460;

    fastcgi_buffer_size 16k;
    fastcgi_buffers 4 32k;
    fastcgi_busy_buffers_size 32k;

    proxy_buffer_size 16k;
    proxy_buffers 4 32k;
    proxy_busy_buffers_size 32k;

    # client body - permission issues
    # https://github.com/dockerfile/nginx/issues/4#issuecomment-209440995
    client_body_temp_path /tmp 1 2;
    client_body_buffer_size 256k;
    client_body_in_file_only off;

    sendfile on;
    tcp_nopush on;
    tcp_nodelay on;

    keepalive_timeout 75 20;
    ignore_invalid_headers on;

    server {
        listen 443 ssl http2;
        server_name "${NGX_HOSTNAME}";
    
        # https://mozilla.github.io/server-side-tls/ssl-config-generator/
        # intermediate profile
    
        # certs sent to the client in SERVER HELLO are concatenated in ssl_certificate
        ssl_certificate /etc/ssl/app/crt_ca.pem;
        ssl_certificate_key /etc/ssl/app/key.pem;
        ssl_session_timeout 1d;
        ssl_session_cache shared:SSL:50m;
        ssl_session_tickets off;
    
        # Diffie-Hellman parameter for DHE ciphersuites, recommended 2048 bits
        ssl_dhparam /etc/ssl/app/dh.pem;
    
        # intermediate configuration. tweak to your needs.
        ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
        ssl_ciphers 'ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA:ECDHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES256-SHA:ECDHE-ECDSA-DES-CBC3-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:DES-CBC3-SHA:!DSS';
        ssl_prefer_server_ciphers on;
    
        # OCSP Stapling - https://raymii.org/s/tutorials/OCSP_Stapling_on_nginx.html
        ssl_stapling on;
        ssl_stapling_verify on;
        resolver 8.8.8.8 8.8.4.4 valid=300s;
        resolver_timeout 5s;
    
        # HSTS - https://raymii.org/s/tutorials/HTTP_Strict_Transport_Security_for_Apache_NGINX_and_Lighttpd.html
        add_header Strict-Transport-Security "max-age=63072000; includeSubdomains; ";

        include /etc/nginx/shibboleth.conf;
    
        root   /var/www/app/web;
    
        location / {
            try_files $uri /app.php$is_args$args;
        }
    
        location ~ ^/(app_dev|config)\.php(/|$) {
            fastcgi_pass ${NGX_APP}:9000;
            fastcgi_split_path_info ^(.+\.php)(/.*)$;
            include fastcgi_params;
            fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
            fastcgi_param DOCUMENT_ROOT $realpath_root;
        }
    
        location ~ ^/app\.php(/|$) {
            fastcgi_pass ${NGX_APP}:9000;
            fastcgi_split_path_info ^(.+\.php)(/.*)$;
            include fastcgi_params;
            fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
            fastcgi_param DOCUMENT_ROOT $realpath_root;
            internal;
        }
    
        location ~ \.php$ {
            return 404;
        }
    
        # error pages
    
        #error_page  404              /404.html;
        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   /usr/share/nginx/html;
        }
    
        # deny access to .htaccess files
        location ~ /\.ht {
            deny  all;
        }
    }
}

# vim: ft=nginx tabstop=4 shiftwidth=4 expandtab :
